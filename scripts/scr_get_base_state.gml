/// scr_get_base_state

// change base state
if (NO_INPUT) {
    base_state = scr_stand_state;
} else if (RIGHT_KEY || LEFT_KEY) {
    base_state = scr_move_state;
} else if (DOWN_KEY) {
    base_state = scr_sit_state;
}
/*} else if (ATTACK_KEY) {
    base_state = scr_attack_state
}*/ 

script_execute(base_state);

