///scr_get_input

RIGHT_KEY = keyboard_check_pressed(MOVE_RIGHT);
LEFT_KEY = keyboard_check_pressed(MOVE_LEFT);
DOWN_KEY = keyboard_check_pressed(MOVE_DOWN);
JUMP_KEY = keyboard_check_pressed(MOVE_JUMP);
ATTACK_KEY = keyboard_check(SWORD_ATTACK);

NO_INPUT = keyboard_check(vk_nokey);

