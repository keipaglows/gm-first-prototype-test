/// scr_get_state

//get keys being pressed
scr_get_input();

// change state
if (NO_INPUT) {
    state = scr_stand_state;
} else if (RIGHT_KEY || LEFT_KEY) {
    state = scr_move_state;
} else if (DOWN_KEY) {
    state = scr_sit_state;
} else if (ATTACK_KEY) {
    state = scr_attack_state
} 

script_execute(state);
